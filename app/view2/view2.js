'use strict';

var starForms = {
  '1' : 'Star',
  'other': 'Stars'
};

var forkForms = {
  '1' : 'Fork',
  'other': 'Forks'
};

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/github/:user', {
    templateUrl: 'view2/view2.html',
    controller: 'UserRepoListCtrl'
  });
}])

.controller('UserRepoListCtrl', ['$scope', '$routeParams', 'githubResource', function($scope, $routeParams, githubResource) {

                  $scope.user_info = githubResource.get({user: $routeParams.user, repo: ''});

                  $scope.publicRepoForms = {
                    '1': 'Public repo',
                    'other': 'Public repos'
                  };
                  $scope.followerForms = {
                    '1': 'Follower',
                    'other': 'Followers'
                  };

                  $scope.repos = githubResource.get({user: $routeParams.user});
                  $scope.user = $routeParams.user;

                  $scope.watchForms = starForms;
                  $scope.forkForms = forkForms;

}]);
