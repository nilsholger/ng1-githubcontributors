'use strict';

angular.module('myApp.view3', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/github/:user/:repo/', {
    templateUrl: 'view3/view3.html',
    controller: 'RepoContributorsListCtrl'
  });
}])

.controller('RepoContributorsListCtrl', ['$scope', '$routeParams', 'githubResource',
function($scope, $routeParams, githubResource) {

          $scope.repoInfo= githubResource.get({
                'query': 'repos',
                'user': $routeParams.user,
                'repo': $routeParams.repo
          });

          $scope.watchForms = starForms;
          $scope.forkForms = forkForms;

          $scope.contributors = githubResource.get({
              'query': 'repos',
              'user' : $routeParams.user,
              'repo' : $routeParams.repo,
              'spec' : 'contributors'
          });
}]);
