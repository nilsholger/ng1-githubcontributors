'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'SearchCtrl'
  });
}])

.controller('SearchCtrl', ['$scope', '$location', function($scope, $location) {

            $scope.user = 'angular';

            $scope.userSearch = function() {
              $location.path(['', 'github', $scope.user, ''].join('/'));
              console.log('usersearch');
            };

}]);
